# project-relogio-ponto

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

O projeto consiste em um registro simples de funcionários e seus respectivos registros ponto desenvolvido em Vue.js.
É possível inserir, alterar e excluir funcionários (CRUD), assim como seus horários de entrada e saída; cada registro
possui a funcionalidade de calcular a carga horária do determinado funcionário após a atualização dos horários de
entrada e saída.

Foi utilizado o framework Bootstrap para o design simplório da aplicação HTML e o código inicia montando o template com a
inserção dos dados Nome, RG e Cidade do funcionário. Para capturar estes daods de formulário a diretiva v-model foi
utilizada e para o registro destes dados foi criado um array denominado idRegister, e, após chamar a função register
utilizando a diretiva v-on:click, que consite na inserção dos dados informados pelo usuário através da função push, uma
variável de controle para capturar o registro dos horários (vControl), um array para armazenar os horários de entrada e
saída (schedules) e uma variável para a carga horária (workload) (criação de um objeto).

Para imprimir o formulário cadastrado e atualizá-lo, a diretiva v-for foi implementada e através dela é possível controlar
o número de funcionários registrados (index) e seus respectivos objetos (emp). Em seguida, o usuário informa os horários
de entrada e saída e estas informações são controladas pela diretiva v-bind:id que atribui um identificador diferente para
cada funcionário registrado através da variável de controle (vControl); isso permite que cada horário registrado seja
associado ao funcionário correto. Quando informado os horários a função scheduleRegister é solicitada e registra estes
no array schedules em duas variáveis: entrada (inTime) e saída (outTime) (criação de outro objeto inserido no objeto
idRegister). A alteração dos dados do primeiro formulário é feita através da diretiva v-model.

A impressão dos horários cadastrados foi realizada através da tag de lista do HTML e da diretiva v-for, já que trata-se de
outro array. O funcionamento de alteração dos horários é similar com a do formulário anterior. Para exclusão dos horário já
cadatrados a função deleteSchedule é solicitada e necessita de dois argumentos (index, sch) para atuar no elemento correto,
relacionado com qual horário será excluído e de qual funcionário. A exclusão ocorre por conta da função splice do
JavaScript, que exige a partir de qual elemento e em quantas posições que atuará, no caso apenas uma. Para deletar
funcionários o procedimento é similar, só exigindo a solicitação da função deleteEmploye.

Finalmente, para o cálculo da carga horária a função calculateWorkload é solicitada através da diretiva v-on:click e com
a funcionalidade forEach executa uma arrow function em cada elemento presente no objeto schedules. A arrow function
consiste em separar em horas e minutos através da função split e dessa forma transforma as strings em valores na base
decimal através da funcionalidade parseInt, sendo possível calcular a diferença entre horas e minutos. Ao final da arrow
function é somado os valores na variável hours e com uma pequena lógica aritmética o valor no formato de string é
armazenado em workload de cada funcionário.